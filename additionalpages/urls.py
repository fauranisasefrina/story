from django.urls import path
from . import views

app_name = 'additionalpages'

urlpatterns = [
    path('organisasi/', views.organisasi, name='organisasi'),
    path('kepanitiaan/', views.kepanitiaan, name='kepanitiaan'),
    path('matakuliah/', views.matakuliah, name ='matakuliah'),
    path('matakuliah/savematakuliah', views.savematakuliah),
    path('showmatakuliah/', views.showmatakuliah, name = 'show'),
    path('showmatakuliah/savematakuliah', views.showmatakuliah),
    path('hapusmatakuliah/<int:id>', views.hapusmatakuliah),
    path('detailmatakuliah/<int:id>', views.detail, name = 'detail'),
    # dilanjutkan ...
]